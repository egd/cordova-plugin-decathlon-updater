package cordova.decathlon.DecathlonUpdater;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import android.os.Environment;
import android.content.Intent;
import android.net.Uri;
import android.content.Context;
import 	android.content.ContextWrapper;
import	java.net.URL;

/**
 * This class echoes a string called from JavaScript.
 */

public class DecathlonUpdater extends CordovaPlugin {
    static int[] maj=new int[]{3,0,0};

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
  
        if (action.equals("install")){       
            Logger.getGlobal().log(Level.WARNING,args.getString(0));     
           
			install(args.getString(0),callbackContext);
           
            return true;
        }
        return false;
    }

 
    private void install(String url,CallbackContext callbackContext){
        try {
             Logger.getGlobal().log(Level.INFO,"Debut de commencement");

            File apk = new File(url);//accès à l'apk courant
            apk=new File(apk.getAbsolutePath());//le convertit en absolu
            Logger.getGlobal().log(Level.WARNING, apk.getAbsolutePath());
            Logger.getGlobal().log(Level.INFO,"File reussi");
            Intent down=new Intent(Intent.ACTION_VIEW);//cree une action a telecharger en aperçu
            down.setDataAndType(Uri.fromFile(apk), "application/vnd.android.package-archive");//le type d'application est un package a telecharger et on fournit le pdf
            Logger.getGlobal().log(Level.INFO,"Intent reussi");
            this.cordova.getActivity().startActivity(down);//le fait installer
            Logger.getGlobal().log(Level.WARNING,url);
        }catch(Exception a){
            callbackContext.error("ARG ! :"+a.getMessage());
        }
    }


  
}
