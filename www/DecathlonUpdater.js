var exec = require('cordova/exec');



module.exports ={

    install:function(arg0,success,error){//télécharge la dernière mise à jour stockée sur le site api-alsolia
        exec(success, error, 'DecathlonUpdater', 'install', [arg0]);
    },
    installHTTP:function(httpURL,success,error){
        window.resolveLocalFileSystemURL(cordova.file.externalRootDirectory+"Download/",function(dir){
            console.log(dir);
            dir.getDirectory("MiseAjourAlsolia",{create:true},function(ci){
            console.log(ci);
            var xhr = new XMLHttpRequest();
            xhr.open('GET', httpURL, true);
            xhr.responseType = 'blob';
            var cimega=ci;
            console.log("Début Téléchargement!");
            xhr.onload = function() {
                if (this.status == 200) {
                    var blob = new Blob([this.response], { type: 'application/vnd.android.package-archive' });
                    console.log("Ecriture dans le répertoire :")
                    console.log(ci);
                                  
                    ci.getFile("lastVersion.apk",{create:true} ,function(file){
                        
                        console.log(file);
                        
                        file.createWriter(function(wr){
                            
                            console.log(wr);
                           
                            wr.onwriteend = function() {
                                console.log("Successful file write... "+ blob );
                                console.log(blob);
                                console.log("Téléchargement terminé");
                                exec(success, error, 'DecathlonUpdater', 'install', ["/storage/emulated/0/Download/MiseAjourAlsolia/lastVersion.apk"]);
                            }
                            wr.onerror = function (e) {
                                console.log("Failed file write: " + e.toString());
                            }
                            wr.write(blob);
                        } ,error   );
                    },error);
                }
            };
            xhr.send();

                                                                  
                
            },error    );
        },error);
    }
}